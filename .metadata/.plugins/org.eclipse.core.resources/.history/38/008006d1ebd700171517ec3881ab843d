withHeaders:
  headers:
    ApplicationName:
      description: This should be calling system name. Used for AUDIT purpose.
      required: false
    ApplicationUser:
      description: This should be calling system username. Used for AUDIT purpose.
      required: false
hasError:
  responses:
    400:
      description: |
        Bad Request:
        The request could not be understood by the server due to malformed syntax or incorrect input. 
        The client SHOULD NOT repeat the request without modifications
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    401:
      description: |
        Unatuthorised:
        The request requires user authentication.
        The response MUST include a WWW-Authenticate header field containing a challenge applicable to the requested resource.
        The client MAY repeat the request with a suitable Authorization header field. 
        If the request already included Authorization credentials, then the 401 response indicates that authorization has been refused for those credentials.
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    403:
        Forbidden:
        The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    404:
      description: |
        Not Found:
        The server has not found anything matching the Request-URI or input parameters. 
        No indication is given of whether the condition is temporary or permanent
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    405:
      description: |
        Method Not Allowed:
        The method specified in the Request-Line is not allowed for the resource identified by the Request-URI.
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    406:
      description: |
        Not Acceptable:
        The resource identified by the request is only capable of generating response entities which have content characteristics not acceptable according to the accept headers sent in the request.
        The client SHOULD NOT repeat the request without modifications.
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    415:
      description: |
        Unsupported Media Type:
        The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method.
        The client SHOULD NOT repeat the request without modifications
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    503:
      description: |
        Service Unavailable:
        The Web server (running the Web site) is currently unable to handle the HTTP request due to a temporary overloading or maintenance of the server.
        The implication is that this is a temporary condition which will be alleviated after some delay. 
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json
    500:
      description: |
        Internal Server Error:
        The server encountered an unexpected condition which prevented it from fulfilling the request.
      body:
        application/json:
          schema: !include jsons/errors/error-response-schema.json